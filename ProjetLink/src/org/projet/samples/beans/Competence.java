package org.projet.samples.beans;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "comPetence")
public class Competence {

	private int id_comp;

	private String nom_comp;

	@XmlElement(name = "idComp", required = false)
	public int getId_comp() {
		return id_comp;
	}

	public void setId_comp(int id_comp) {
		this.id_comp = id_comp;
	}

	@XmlElement(name = "nomComp")
	public String getNom_comp() {
		return nom_comp;
	}

	public void setNom_comp(String nom_comp) {
		this.nom_comp = nom_comp;
	}

}
