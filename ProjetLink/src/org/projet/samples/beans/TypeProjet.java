package org.projet.samples.beans;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "typeProjet")
public class TypeProjet {

	private int id_projet;

	private String nom_projet;

	@XmlElement(name = "idProjet", required = false)
	public int getId_projet() {
		return id_projet;
	}

	public void setId_projet(int id_projet) {
		this.id_projet = id_projet;
	}

	@XmlElement(name = "nomProjet", required = true)
	public String getNom_projet() {
		return nom_projet;
	}

	public void setNom_projet(String nom_projet) {
		this.nom_projet = nom_projet;
	}

}
