package org.projet.samples.dao;

import java.util.List;

import org.projet.samples.beans.Competence;
import org.projet.samples.service.IService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {
	private static IService service;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String chemin = "applicationContext-jdbc.xml";

		ApplicationContext context = new ClassPathXmlApplicationContext(chemin);
		service = (IService) context.getBean("service");

		// TypeProjet projet = service.getTyprojet(1);
		List<Competence> list = service.getCompofprojet(3);

		for (Competence p : list) {
			System.out.println(p.getNom_comp());
		}

	}
}
