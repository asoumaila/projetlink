package org.projet.samples.service;

import java.util.List;

import org.projet.samples.beans.Competence;
import org.projet.samples.beans.Competences;
import org.projet.samples.beans.TypeProjet;
import org.projet.samples.beans.TypeProjets;

public interface IService {

	public TypeProjet getTyprojet(int id);

	public void addprojet(TypeProjet projet);

	public TypeProjet getProjetByName(String name);

	public Competence getComp(int id);

	public Competence getCompByName(String name);

	public void addComp(Competence competence);

	public TypeProjets getAllProjet();

	public Competences getAllComp();

	public List<Competence> getCompofprojet(int id_projet);

	public List<Competence> getCompNotProjet(int id);

	public void linkProjComp(int id_projet, int id_comp);

}
